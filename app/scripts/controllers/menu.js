'use strict';

angular.module('parkingSystem')
  .controller('MenuCtrl', function ($scope, $route) {
    	
    	$scope.businessid = $route.current.params.businessid;

    	$scope.items = [];

    	$.get('http://nixin-swbay.rhcloud.com/items', function(data, error){
    		
    		$scope.items = data;
    		$scope.$apply();
    	});

  });
