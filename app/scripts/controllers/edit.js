'use strict';

angular.module('parkingSystem')
  .controller('EditCtrl', function ($scope, $route) {
    
  	// Fetch the data from the CatalogApi
    $.get('http://tmforum-test.apigee.net/v1/tm-forum-product-offering-api/' + $route.current.params.businessid, function(d){

    		console.log(d);
    		$scope.username = d["name"];
    		var hack = d["description"].split("|");
    		$scope.long =hack[0];
    		$scope.magn =hack[1];
    		$scope.nbspots =hack[2];
    		$scope.price = d["productOfferingPrices"][0].price.amount;
    		$scope.$digest();

    });

    $scope.submit = function(){ // = update account
    	// Well, not implemented in the CatalogManager... to bad.
    	$.ajax({
  			type: "PUT",
  			url: 'http://tmforum-test.apigee.net/v1/tm-forum-product-offering-api/' + $route.current.params.businessid, 
  			data: {
    		"name": $scope.username,
			"description": $scope.long + "|" + $scope.magn + "|"  + $scope.nbspots,
			"isBundle": false,
			"productCategories": [
				{
					"name": "parking-lot"
				}
			],
			"productOfferingPrices": [
				{
				    "name": "Parking price",
				    "description": "Parking price",
				    "priceType": "unique",
				    "price": {
				    	"amount": $scope.amount,
				    	"currency": "USD"
				    }
			    }
			]
    	}, 
    	success: function(d){
    		// We could say hi...
    	}
    });
  };

});
