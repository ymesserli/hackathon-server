'use strict';

angular.module('parkingSystem')
  .controller('FeaturesCtrl', function ($scope, $route, $filter) {
	
	$scope.businessid = $route.current.params.businessid;

	$scope.features = [
		{
			name: 'Menu Display',
			id: 'menu_display',
			price: 1,
			idnb: 1,
			url: 'menu',
			checked: $scope.businessid == 'test'
		},
		{
			name: 'Buzzer',
			id: 'buzzer',
			price: 0,
			idnb: 2,
			checked: true,
		},
		{
			name: 'Favorites',
			id: 'Favorites',
			price: 1,
			idnb: 3
		},
		{
			name: 'Ordering',
			id: 'ordering',
			price: 5,
			idnb: 3
		},
		{
			name: 'Cart',
			id: 'Cart',
			price: 5,
			idnb: 3
		},
		{
			name: 'Time Estimation',
			id: 'Estimation',
			price: 5,
			idnb: 3
		},
		{
			name: 'Daily Menu',
			id: 'Menu',
			price: 5,
			idnb: 3
		},
		{
			name: 'Specialities',
			id: 'Specialities',
			price: 5,
			idnb: 3
		},
		{
			name: 'Paying',
			id: 'Paying',
			price: 10,
			idnb: 3
		},
		{
			name: 'Split the bill',
			id: 'split',
			price: 20,
			idnb: 3
		},
		{
			name: 'Timely orders',
			id: 'orders',
			price: 20,
			idnb: 3
		},
		{
			name: 'Loyalty Programs',
			id: 'Programs',
			price: 20,
			idnb: 3
		},
		{
			name: '24/7 Phone Support',
			id: 'Support',
			price: 30,
			idnb: 3
		},
		{
			name: 'Analytics',
			id: 'Analytics',
			price: 30,
			idnb: 3
		},
		{
			name: 'Coupons',
			id: 'Coupons',
			price: 30,
			idnb: 3
		},
		{
			name: 'POS integration',
			id: 'integration',
			price: 50,
			idnb: 3
		},
		{
			name: 'Next Day Support',
			id: 'Support',
			price: 150,
			idnb: 3
		}

	];

	$scope.tot = 0;

	$scope.selected = function () {
	    	$scope.selected_features = $filter('filter')($scope.features, {checked: true});
	    	computeTot();
	    	$scope.$apply();
	}

	var computeTot = function(){
		$scope.tot = 0;
		$.each($scope.features, function(i, c){ console.log(c); if(c.checked) $scope.tot += c.price; });
	};

	computeTot();
  });
