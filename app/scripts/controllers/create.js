'use strict';

angular.module('parkingSystem')
  .controller('CreateCtrl', function ($scope, $location) {

    $scope.submit = function(){ // = create account
    	// Problem with jquery that encode all the variables.
    	// Doesnt work.
    	$.ajax({
  			type: "POST",
  			url: 'http://tmforum-test.apigee.net/v1/tm-forum-product-offering-api/', 
  			data: {
    		"name": $scope.username,
			"description": $scope.long + "|" + $scope.magn + "|" + $scope.nbspots,
			"isBundle": false,
			"productCategories": [
				{
					"name": $scope.username + "-parking-lot"
				}
			],
			"productOfferingPrices": [
				{
				    "name": "Parking price",
				    "description": "Parking price",
				    "priceType": "unique",
				    "price": {
				    	"amount": $scope.amount,
				    	"currency": "USD"
				    }
			    }
			]
    	}, 
    	success: function(d){
    		 window.location = '/#/' + $scope.username
    	}
    });
  }});
