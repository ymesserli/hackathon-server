'use strict';

angular.module('parkingClientApp', ['ngResource'])



  .factory('ProductResource', function($resource){
//    return $resource('http://64.103.39.150\\:4848/cisco/tmf/productOffering/:id',
    return $resource('http://tmforum-test.apigee.net/v1/tm-forum-product-offering-api/:id',
      {id: '@id'},
      {getOffering: {method: 'GET', params: {id: 0, 'productCategories.name':  buinessid + '-parking-lot' }}},
      {queryOffering: {method: 'GET', isArray:true, params: {id: 0}}}
    )
  })
