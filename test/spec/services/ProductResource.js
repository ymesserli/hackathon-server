'use strict';

describe('Service: ProductResource', function () {

  // load the service's module
  beforeEach(module('parkingClientApp'));

  // instantiate service
  var ProductResource;
  beforeEach(inject(function (_ProductResource_) {
    ProductResource = _ProductResource_;
  }));

  it('should do something', function () {
    expect(!!ProductResource).toBe(true);
  });

});
